package velez.loor.javier.vicioapp.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import velez.loor.javier.vicioapp.Adaptadores.AdaptadorCompras;
import velez.loor.javier.vicioapp.BaseDeDatos.PedidosDB;
import velez.loor.javier.vicioapp.R;

public class ComprasActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdaptadorCompras adaptadorCompras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compras);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView)findViewById(R.id.RecyclerCompras);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        PedidosDB pedidosDB = new PedidosDB(this);
        adaptadorCompras = new AdaptadorCompras(pedidosDB.comprasList());
        recyclerView.setAdapter(adaptadorCompras);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
