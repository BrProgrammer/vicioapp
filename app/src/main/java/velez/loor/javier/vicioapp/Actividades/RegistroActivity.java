package velez.loor.javier.vicioapp.Actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import velez.loor.javier.vicioapp.BaseDeDatos.UsuariosDB;
import velez.loor.javier.vicioapp.Modelos.Usuarios;
import velez.loor.javier.vicioapp.R;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener{

    private Button registrarse, iniciar;
    private EditText nombres, apellidos, celular, direccion, correo, clave;
    private UsuariosDB usuariosDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombres = (EditText)findViewById(R.id.TxtNombres);
        apellidos = (EditText)findViewById(R.id.TxtApellidos);
        celular = (EditText)findViewById(R.id.TxtCelular);
        direccion = (EditText)findViewById(R.id.TxtDireccion);
        correo = (EditText)findViewById(R.id.TxtCorreo);
        clave = (EditText)findViewById(R.id.TxtClave);
        registrarse = (Button)findViewById(R.id.BtnRegistro);
        iniciar = (Button)findViewById(R.id.BtnLoginActivity);
        usuariosDB = new UsuariosDB(this);

        iniciar.setOnClickListener(this);
        registrarse.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BtnLoginActivity:
                finish();
                break;


            case R.id.BtnRegistro:
                String s = "DEBE INSERTAR SUS ";
                String s2 = "DEBE INSERTAR SU ";
                if (nombres.getText().toString().isEmpty()){
                    MostrarToast(s + "NOMBRES");
                    nombres.requestFocus();
                }else if (apellidos.getText().toString().isEmpty()){
                    MostrarToast(s +"APELLIDOS");
                    apellidos.requestFocus();
                }else if (celular.getText().toString().isEmpty()){
                    MostrarToast(s2 + "CELULAR");
                    celular.requestFocus();
                }else if (direccion.getText().toString().isEmpty()){
                    MostrarToast(s2 + "DIRECCION");
                    direccion.requestFocus();
                }else if (correo.getText().toString().isEmpty()){
                    MostrarToast(s2 + "CORREO");
                    correo.requestFocus();
                }else if (clave.getText().toString().isEmpty()){
                    MostrarToast(s2 + "CLAVE");
                    clave.requestFocus();
                }else {
                    Usuarios usuarios = new Usuarios();
                    usuarios.setNombres(nombres.getText().toString().trim());
                    usuarios.setApellidos(apellidos.getText().toString().trim());
                    usuarios.setCelular(celular.getText().toString().trim());
                    usuarios.setDireccion(direccion.getText().toString().trim());
                    usuarios.setCorreo(correo.getText().toString().trim());
                    usuarios.setClave(clave.getText().toString().trim());
                    if (usuariosDB.RegistrarUsuario(usuarios)){
                        MostrarToast("REGISTRO EXITOSO");
                        finish();
                    }else {
                        MostrarToast("EL USUARIO YA EXISTE");
                    }
                }
                break;
        }

    }

    private void MostrarToast(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
