package velez.loor.javier.vicioapp.SharedPreff;

import android.content.Context;
import android.content.SharedPreferences;

public class Sesion {

    public static final String SHARED_PREF_NAME = "MI_SESION_ACTIVA";

    //Username
    public static final String USER_EMAIL = "correo";
    public static Sesion mInstance;
    public static Context mCtx;


    public Sesion(Context context) {
        mCtx = context;
    }


    public static synchronized Sesion getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new Sesion(context);
        }
        return mInstance;
    }

    public void storeUserName(String email) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_EMAIL, email);
        editor.commit();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(USER_EMAIL, null) != null;
    }
}
