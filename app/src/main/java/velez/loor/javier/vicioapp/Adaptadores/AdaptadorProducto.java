package velez.loor.javier.vicioapp.Adaptadores;

import android.content.Intent;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import velez.loor.javier.vicioapp.Actividades.DetalleActivity;
import velez.loor.javier.vicioapp.Modelos.Productos;
import velez.loor.javier.vicioapp.R;

public class AdaptadorProducto extends RecyclerView.Adapter<AdaptadorProducto.MyViewHolder>{

    private List<Productos> productosList;

    public AdaptadorProducto(List<Productos> productosList) {
        this.productosList = productosList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_merca, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.textView.setText(productosList.get(position).getNombreP());
        holder.imageView.setImageResource(productosList.get(position).getImagenP());
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Productos productos = productosList.get(position);
                Intent intent = new Intent(v.getContext(), DetalleActivity.class);
                intent.putExtra("Nombre", productos.getNombreP());
                intent.putExtra("Precio", productos.getPrecioP());
                intent.putExtra("Descuento", productos.getDescuentoP());
                intent.putExtra("Id", productos.getIdP());
                intent.putExtra("Imagen", productos.getImagenP());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productosList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView textView;
        private View view;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            imageView = (ImageView)view.findViewById(R.id.imagen_p);
            textView = (TextView)view.findViewById(R.id.nombre_p);
        }
    }

}
