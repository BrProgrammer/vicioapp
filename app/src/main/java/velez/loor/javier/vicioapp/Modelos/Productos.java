package velez.loor.javier.vicioapp.Modelos;

public class Productos {

    private String nombreP, precioP, descuentoP;
    private int imagenP, idP;

    public Productos() {
    }

    public Productos(String nombreP, String precioP, String descuentoP, int imagenP, int idP) {
        this.nombreP = nombreP;
        this.precioP = precioP;
        this.descuentoP = descuentoP;
        this.imagenP = imagenP;
        this.idP = idP;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public String getPrecioP() {
        return precioP;
    }

    public void setPrecioP(String precioP) {
        this.precioP = precioP;
    }

    public String getDescuentoP() {
        return descuentoP;
    }

    public void setDescuentoP(String descuentoP) {
        this.descuentoP = descuentoP;
    }

    public int getImagenP() {
        return imagenP;
    }

    public void setImagenP(int imagenP) {
        this.imagenP = imagenP;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }
}
