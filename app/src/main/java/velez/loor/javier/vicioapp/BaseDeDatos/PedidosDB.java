package velez.loor.javier.vicioapp.BaseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import velez.loor.javier.vicioapp.Modelos.Compras;

public class PedidosDB extends SQLiteOpenHelper {

    private static final String NombreBD = "PRODUCTOS_PEDIDOS";
    private static final int VersionBD = 1;
    private String NombreTabla = "PEDIDOS";
    private String NombreP = "Nombre_Producto";
    private String Cantidad = "Cantidad_Producto";
    private String PrecioP = "Precio_Producto";
    private String SubTotal = "SubTotal";
    private String Descuento = "Descuento";
    private String Total = "Total";
    private String IMAGEN = "Imagen";
    private String Tabla = "CREATE TABLE " + NombreTabla + "( id integer PRIMARY KEY AUTOINCREMENT, " + NombreP + " text, "
            + Cantidad + " text, "+ PrecioP + " text, " + SubTotal + " text, " + Descuento + " text, " + Total + " text, " + IMAGEN + " integer)";

    public PedidosDB(@Nullable Context context) {
        super(context, NombreBD, null, VersionBD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Tabla);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Tabla);
        db.execSQL(Tabla);
    }

    public void GuardarPedidos(String nombreP, String CantidadP, String Precio, String Subtotal, String Descuentoo, String Totall, int imagen){
        SQLiteDatabase database = getWritableDatabase();
        if (database!=null){
            ContentValues contentValues = new ContentValues();
            contentValues.put(NombreP, nombreP);
            contentValues.put(Cantidad, CantidadP);
            contentValues.put(PrecioP, Precio);
            contentValues.put(SubTotal, Subtotal);
            contentValues.put(Descuento, Descuentoo);
            contentValues.put(Total, Totall);
            contentValues.put(IMAGEN, imagen);
            database.insert(NombreTabla, null, contentValues);
            database.close();
        }
    }

    public List<Compras> comprasList(){
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + NombreTabla, null);
        List<Compras> comprasList = new ArrayList<>();
        if (cursor.moveToFirst()){
            do {
                Compras compras = new Compras();
                compras.setId(cursor.getInt(0));
                compras.setNombre(cursor.getString(1));
                compras.setCantidad(cursor.getString(2));
                compras.setPrecio(cursor.getString(3));
                compras.setSubtotal(cursor.getString(4));
                compras.setDescuento(cursor.getString(5));
                compras.setTotal(cursor.getString(6));
                compras.setImagen(cursor.getInt(7));
                comprasList.add(compras);

            }while (cursor.moveToNext());
        }
        return comprasList;
    }

}
