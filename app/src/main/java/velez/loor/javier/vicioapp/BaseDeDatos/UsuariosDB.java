package velez.loor.javier.vicioapp.BaseDeDatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import velez.loor.javier.vicioapp.Modelos.Usuarios;

public class UsuariosDB {

    public int Mensaje;
    private Context context;
    private Usuarios usuarios;
    private ArrayList<Usuarios> usuariosArrayList;
    private SQLiteDatabase liteDatabasel;
    private String NombreDB = "USUARIOS_DB";
    private String NombreTabla = "USUARIOS";
    private String Nombres = "Nombres";
    private String Apellidos = "Apellidos";
    private String Celular = "Celular";
    private String Direccion = "Direccion";
    private String Correo = "Correo";
    private String Clave = "Clave";
    private String Tabla = "CREATE TABLE IF NOT EXISTS " + NombreTabla + "( id integer PRIMARY KEY AUTOINCREMENT, " + Nombres + " text, " + Apellidos + " text, "+
            Celular + " text, " + Direccion + " text, " + Correo + " text, " + Clave + " text)";

    public UsuariosDB(Context context) {
        Mensaje = 0;
        this.context = context;
        liteDatabasel= context.openOrCreateDatabase(NombreDB, context.MODE_PRIVATE, null);
        liteDatabasel.execSQL(Tabla);
        usuarios = new Usuarios();
    }

    public boolean RegistrarUsuario(Usuarios usuarios){
        if (UsuarioExiste(usuarios.getCorreo())==0){
            ContentValues contentValues = new ContentValues();
            contentValues.put(Nombres, usuarios.getNombres());
            contentValues.put(Apellidos, usuarios.getApellidos());
            contentValues.put(Celular, usuarios.getCelular());
            contentValues.put(Direccion, usuarios.getDireccion());
            contentValues.put(Correo, usuarios.getCorreo());
            contentValues.put(Clave, usuarios.getClave());
            return (liteDatabasel.insert(NombreTabla, null, contentValues)>0);
        }else {
            return false;
        }
    }

    public int UsuarioExiste(String correo){
        int i = 0;
        usuariosArrayList = SeleccionarUsuario();
        for (Usuarios usuarios: usuariosArrayList){
            if (usuarios.getCorreo().equals(correo)){
                i++;
            }
        }
        return i;
    }

    public ArrayList<Usuarios> SeleccionarUsuario(){
        ArrayList<Usuarios> usuariosArrayList1 = new ArrayList<>();
        usuariosArrayList1.clear();
        Cursor cursor = liteDatabasel.rawQuery("SELECT * FROM " + NombreTabla, null);
        if (cursor!=null && cursor.moveToFirst()){
            do {
                Usuarios usuarios1 = new Usuarios();
                usuarios1.setId(cursor.getInt(0));
                usuarios1.setNombres(cursor.getString(1));
                usuarios1.setApellidos(cursor.getString(2));
                usuarios1.setCelular(cursor.getString(3));
                usuarios1.setDireccion(cursor.getString(4));
                usuarios1.setCorreo(cursor.getString(5));
                usuarios1.setClave(cursor.getString(6));
                usuariosArrayList1.add(usuarios1);
            }while (cursor.moveToNext());
        }
        return usuariosArrayList1;
    }

    public void Login(String correo, String clave){
        Mensaje = 0;
        Cursor cursor = liteDatabasel.rawQuery("SELECT * FROM " + NombreTabla, null);
        if (cursor!=null && cursor.moveToFirst()){
            do {
                if (cursor.getString(5).equals(correo) && cursor.getString(6).equals(clave)){
                    Mensaje=1;
                }else if (cursor.getString(5).equals(correo) && cursor.getString(6)!= clave){
                    Mensaje=2;
                }else if (cursor.getString(5)!= correo && cursor.getString(6).equals(clave)){
                    Mensaje=3;
                }else {
                    Mensaje=4;
                }
            }while (cursor.moveToNext());
        }
    }
}
