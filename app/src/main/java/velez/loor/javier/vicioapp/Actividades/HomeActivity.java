package velez.loor.javier.vicioapp.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import velez.loor.javier.vicioapp.Adaptadores.AdaptadorProducto;
import velez.loor.javier.vicioapp.MainActivity;
import velez.loor.javier.vicioapp.Modelos.Productos;
import velez.loor.javier.vicioapp.R;
import velez.loor.javier.vicioapp.SharedPreff.Sesion;

import static velez.loor.javier.vicioapp.SharedPreff.Sesion.mCtx;

public class HomeActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private AdaptadorProducto adaptadorProducto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        recyclerView = (RecyclerView)findViewById(R.id.Recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adaptadorProducto = new AdaptadorProducto(productosList());
        recyclerView.setAdapter(adaptadorProducto);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.carrito:
                startActivity(new Intent(this, ComprasActivity.class));
                return true;

            case R.id.salir:
                Sesion sesion = new Sesion(this);
                SharedPreferences sharedPreferences = getSharedPreferences(sesion.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                startActivity(new Intent(this, MainActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    private List<Productos> productosList(){
        List<Productos> productosArrayLis = new ArrayList<>();
        productosArrayLis.add(new Productos("Parecetamol", "2.5", "7", R.drawable.paracetamol, 1));
        productosArrayLis.add(new Productos("Aspirinas", "3.00", "1", R.drawable.aspirinas, 2));
        productosArrayLis.add(new Productos("Finalin", "4.00", "2", R.drawable.finalin, 3));

        return productosArrayLis;
    }
}
