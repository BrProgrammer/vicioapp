package velez.loor.javier.vicioapp.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import velez.loor.javier.vicioapp.BaseDeDatos.PedidosDB;
import velez.loor.javier.vicioapp.MainActivity;
import velez.loor.javier.vicioapp.R;

public class DetalleActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView nombre, precio, descuento;
    private Spinner spinner;
    private Button button;
    private String Can;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        imageView = (ImageView)findViewById(R.id.IMGPrueba);
        nombre = (TextView) findViewById(R.id.LBLNombreP);
        precio = (TextView)findViewById(R.id.LBLPrecio);
        descuento = (TextView)findViewById(R.id.LBLDescuento);

        imageView.setImageResource(getIntent().getExtras().getInt("Imagen"));
        precio.setText("Precio: "+getIntent().getStringExtra("Precio"));
        descuento.setText("Descuento: "+getIntent().getStringExtra("Descuento")+"%");
        nombre.setText(getIntent().getStringExtra("Nombre"));

        button = (Button)findViewById(R.id.BtnComprar);
        spinner = (Spinner)findViewById(R.id.SPinnerCantidad);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Can = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Can.equals("CANTIDAD")){
                    Toast.makeText(DetalleActivity.this, "DEBE ELEGIR LA CANTIDAD", Toast.LENGTH_SHORT).show();
                }else {
                    Double precio = Double.valueOf(getIntent().getStringExtra("Precio"));
                    Double descuento = Double.valueOf(getIntent().getStringExtra("Descuento"));
                    Double cantidad = Double.valueOf(Can);

                    Double Subt = precio*cantidad;
                    Double d = descuento/100.00;
                    Double Des = Subt*d;
                    Double Total = Subt - Des;
                    PedidosDB pedidosDB =  new PedidosDB(DetalleActivity.this);
                    pedidosDB.GuardarPedidos(getIntent().getStringExtra("Nombre"), Can, getIntent().getStringExtra("Precio"),
                            Subt.toString(), Des.toString(), Total.toString(), getIntent().getExtras().getInt("Imagen"));
                    Toast.makeText(DetalleActivity.this, "COMPRADO", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

    }
}
