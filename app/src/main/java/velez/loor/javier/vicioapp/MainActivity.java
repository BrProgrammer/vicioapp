package velez.loor.javier.vicioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import velez.loor.javier.vicioapp.Actividades.HomeActivity;
import velez.loor.javier.vicioapp.Actividades.RegistroActivity;
import velez.loor.javier.vicioapp.BaseDeDatos.UsuariosDB;
import velez.loor.javier.vicioapp.SharedPreff.Sesion;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private Button login, register;
    private EditText correo, clave;
    private UsuariosDB usuariosDB;
    private Sesion sesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sesion = new Sesion(this);

        if (sesion.isLoggedIn()){
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }else {
            login = (Button)findViewById(R.id.BtnLogin);
            register = (Button)findViewById(R.id.BtnRegistarseActivity);
            correo = (EditText)findViewById(R.id.TxtCorreoLogin);
            clave = (EditText)findViewById(R.id.TxtClaveLogin);
            usuariosDB = new UsuariosDB(this);
            login.setOnClickListener(this);
            register.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.BtnLogin:
                if (correo.getText().toString().isEmpty()){
                    ToastMostrar("CORREO NECESARIO");
                    correo.requestFocus();
                }else if (clave.getText().toString().isEmpty()){
                    ToastMostrar("CLAVE NECESARIO");
                    clave.requestFocus();
                }else {
                    usuariosDB.Login(correo.getText().toString().trim(), clave.getText().toString().trim());
                    if (usuariosDB.Mensaje == 1) {
                        Sesion.getInstance(this).storeUserName(correo.getText().toString().trim());
                        ToastMostrar("BIENVENIDO");
                        startActivity(new Intent(this, HomeActivity.class));
                        finish();
                    } else if (usuariosDB.Mensaje == 2) {
                        ToastMostrar("CLAVE INCORRECTA, ESCRIBA CORRECTAMENTE");
                    } else if (usuariosDB.Mensaje == 3) {
                        ToastMostrar("CORREO INCORRECTO, ESCRIBA CORRECTAMENTE");
                    } else if (usuariosDB.Mensaje == 4) {
                        ToastMostrar("USUARIO NO REGISTRADO");
                    }
                }
                break;

            case R.id.BtnRegistarseActivity:
                correo.setText("");
                clave.setText("");
                startActivity(new Intent(this, RegistroActivity.class));
                break;

        }
    }

    private void ToastMostrar(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
