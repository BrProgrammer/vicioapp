package velez.loor.javier.vicioapp.Adaptadores;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

import velez.loor.javier.vicioapp.Modelos.Compras;
import velez.loor.javier.vicioapp.R;

public class AdaptadorCompras extends RecyclerView.Adapter<AdaptadorCompras.Vista>{

    private List<Compras> comprasArrayList;

    public AdaptadorCompras(List<Compras> comprasArrayList) {
        this.comprasArrayList = comprasArrayList;
    }

    @NonNull
    @Override
    public Vista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Vista(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_compras, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Vista holder, int position) {
        Compras compras = comprasArrayList.get(position);
        holder.nombre.setText("Medicamento: "+compras.getNombre());
        holder.cantidad.setText("Cantidad: "+compras.getCantidad());
        holder.total.setText("Total: "+compras.getTotal());
        holder.img.setImageResource(compras.getImagen());
    }

    @Override
    public int getItemCount() {
        return comprasArrayList.size();
    }


    public static class Vista extends RecyclerView.ViewHolder{

        private ImageView img;
        private TextView nombre, cantidad, total;
        public Vista(@NonNull View itemView) {
            super(itemView);
            img = (ImageView)itemView.findViewById(R.id.ImagPC);
            cantidad = (TextView)itemView.findViewById(R.id.LBLCantidadPC);
            total = (TextView)itemView.findViewById(R.id.LBLTotalPC);
            nombre = (TextView)itemView.findViewById(R.id.LBLNombrePC);
        }
    }
}
